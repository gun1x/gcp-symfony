#!/bin/bash
export current_version="$(gcloud container images list-tags gcr.io/test-project-158316/nginx_test | head -2 | tail -1 | awk '{ print $2 }' | rev | cut -d',' -f 1 | rev)"
if [[ $1 == major_bump ]]; then
    echo 0.$(( `echo $current_version | cut -d"." -f2` + 1)).1
elif [[ $1 == minor_bump ]]; then
    echo `echo $current_version | cut -d"." -f1-2`.$(( `echo $current_version | cut -d"." -f3` + 1 ))
elif [[ $1 == current ]]; then
    echo $current_version
fi
 
